package com.safaricom.config;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class TestConfig {
	private static TestConfig instance = null;

	private AppiumDriver<MobileElement> driver;
	private WebDriverWait wait;
//	ExtentReports extentReport;

	private TestConfig() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "Redmi");
		capabilities.setCapability("udid", "3300e1dd1655638d");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion", "6.0.1"); // com.selfcare.safaricom/com.mventus.selfcare.safaricom.activity.SplashScreenActivity
		capabilities.setCapability("appPackage", "com.selfcare.safaricom");
		capabilities.setCapability("appActivity", "com.mventus.selfcare.safaricom.activity.SplashScreenActivity");
		// capabilities.setCapability("skipUnlock", "true");
		capabilities.setCapability("noReset", "false");
		capabilities.setCapability("unicodeKeyboard", "false");
		capabilities.setCapability("resetKeyboard", "true");
		try {
			driver = new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			wait = new WebDriverWait(driver, 120);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static TestConfig getInstance() {
		if (instance == null) {
			instance = new TestConfig();
		}
		return instance;
	}

	public WebDriverWait getWait() {
		return wait;
	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}
}
