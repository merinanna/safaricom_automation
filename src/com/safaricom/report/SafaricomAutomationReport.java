package com.safaricom.report;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.safaricom.config.TestConfig;
import com.safaricom.login.LoginTest;
import com.safaricom.mpesa.BillManager;
import com.safaricom.mpesa.BuyAirtime;
import com.safaricom.mpesa.FulizaMPESA;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.mpesa.LoansandSavings;
import com.safaricom.mpesa.MPESAGlobal;
import com.safaricom.mpesa.MyAccount;
import com.safaricom.mpesa.SendMoney;
import com.safaricom.mpesa.WithdrawCash;
import com.safaricom.mpesa.mpesaHomepage;
import com.safaricom.services.BongaServices;
import com.safaricom.services.DataAndSMSPlans;
import com.safaricom.services.FlexServices;
import com.safaricom.services.MySMSServices;
import com.safaricom.services.ServicesHome;
import com.safaricom.sfc.Checkbalance;
import com.safaricom.sfc.sfcHomepage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SafaricomAutomationReport {
	ExtentReports extentReport;
	ExtentTest extentTest;
	AppiumDriver<MobileElement> driver;
	public static LoginTest loginObject = new LoginTest();
	public static mpesaHomepage mpesahomeObject = new mpesaHomepage();
	public static sfcHomepage sfchomeObject = new sfcHomepage();
	public static SendMoney sendMoneyObject = new SendMoney(); // Create object for classes
	public static WithdrawCash withdrawCashObject = new WithdrawCash();
	public static BuyAirtime buyAirtimeObject = new BuyAirtime();
	public static LipanaMpesa lipanaMpesaObject = new LipanaMpesa();
	public static BillManager billmanagerObject = new BillManager();
	public static LoansandSavings loanssavingsObject = new LoansandSavings();
	public static MyAccount myAccountObject = new MyAccount();
	public static FulizaMPESA FulizaMPESAObject = new FulizaMPESA();
	public static MPESAGlobal MPESAGlobalObject = new MPESAGlobal();
	public static Checkbalance CheckbalanceObject=new Checkbalance();
	public static ServicesHome ServiceHomeObject=new ServicesHome();
	public static BongaServices BongaServicesObject=new BongaServices();
	public static FlexServices FlexServicesObject=new FlexServices();
	public static DataAndSMSPlans DataAndSMSPlansObject=new DataAndSMSPlans();
	public static MySMSServices MySMSServicesObject=new MySMSServices();

	@BeforeSuite
	public void beforeSuite() {
//In before suite we are creating HTML report template, adding basic information to it and load the extent-config.xml file

		extentReport = new ExtentReports(System.getProperty("user.dir") + "/Safaricom_AutomationReport.html", true);
		extentReport.addSystemInfo("Host Name", "Safaricom").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Merin");
		extentReport.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
	}

	@BeforeClass
	public void beforeClass() {
		// WebDriverWait wait = TestConfig.getInstance().getWait();
		// AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
//In before method we are collecting the current running test case name
		String className = this.getClass().getSimpleName();
		extentTest = extentReport.startTest(className + "-" + method.getName());
	}

	@Test(priority = 0)
	public void maintest() throws InterruptedException, IOException {
		loginObject.LoginSuccess();
		sfchomeObject.sfcHomePageSuccess();
		Assert.assertTrue(true);
	}

//	@Test(priority = 2)
//	public void mpesahometest() throws InterruptedException, IOException {
//		mpesahomeObject.mpesaHomePageSuccess();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 3)
//	public void sendmoneytest() throws InterruptedException, IOException {
//		sendMoneyObject.SendMoneySucces();// call SendMoneySucces function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 4)
//	public void withdrawCashtest() throws InterruptedException, IOException {
//		withdrawCashObject.WithdrawCashSuccess();// call WithdrawCashSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 5)
//	public void BuyAirtimetest() throws InterruptedException, IOException {
//		buyAirtimeObject.BuyAirtimeSuccess();// call BuyAirtimeSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 6)
//	public void LipanaMPesatest() throws InterruptedException, IOException {
//		lipanaMpesaObject.LipanaMPesaSuccess();// call LipanaMPesaSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 7)
//	public void BillManagertest() throws InterruptedException, IOException {
//		billmanagerObject.BillManagerSuccess(); // call BillManagerSuccess function
//		Assert.assertTrue(true);
//
//	}
//
//	@Test(priority = 8)
//	public void LoansandSavingstest() throws InterruptedException, IOException {
//		loanssavingsObject.LoansandSavingsSuccess();// call LoansandSavingsSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 9)
//	public void MyAccounttest() throws InterruptedException, IOException {
//		myAccountObject.MyAccountSuccess();// call MyAccountSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 10)
//	public void FulizaMPESAtest() throws InterruptedException, IOException {
//		FulizaMPESAObject.FulizaMPESASucces();// call LoansandSavingsSuccess function
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority = 11)
//	public void MPESAGlobaltest() throws InterruptedException, IOException {
//		MPESAGlobalObject.MPESAGlobalSucces();// call MyAccountSuccess function
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority = 1)
//	public void CheckBalancetest() throws InterruptedException, IOException {
//		CheckbalanceObject.CheckbalanceSuccess();// call CheckBalanceSuccess function
//		Assert.assertTrue(true);
//	}

	
	
	@Test(priority = 1)
	public void ServicesHometest() throws InterruptedException, IOException {
		ServiceHomeObject.ServicesHomePageSuccess();// call ServicesHomePageSuccess function
		Assert.assertTrue(true);
	}
//	
//	@Test(priority = 2)
//	public void BongaServicestest() throws InterruptedException, IOException {
//		BongaServicesObject.BongaServicesSuccess();// call ServicesHomePageSuccess function
//		Assert.assertTrue(true);
//	}
//	@Test(priority = 3)
//	public void FlexServicestest() throws InterruptedException, IOException {
//		FlexServicesObject.FlexServicesSuccess();// call ServicesHomePageSuccess function
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority = 4)
//	public void DataAndSMSPlanstest() throws InterruptedException, IOException {
//		DataAndSMSPlansObject.DataAndSMSPlansSuccess();// call ServicesHomePageSuccess function
//		Assert.assertTrue(true);
//	}
//	
//	
//	@Test(priority = 5)
//	public void MySMSServicetest() throws InterruptedException, IOException {
//		MySMSServicesObject.MySMSServicesSuccess();// call ServicesHomePageSuccess function
//		Assert.assertTrue(true);
//	}
	
	
	
	

//	@Test
//	public void skipTest() {
//		throw new SkipException("Skipping – This is not ready for testing ");
//	}

	@AfterMethod
	public void getResult(ITestResult result, Method method) throws Exception {
//In after method we are collecting the test execution status and based on that the information writing to HTML report
		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "Test Case Failed is : " + result.getName());
			extentTest.log(LogStatus.FAIL, "Error Details :-" + result.getThrowable().getMessage());
		}
		if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case Skipped is :  " + result.getName());
		}
		if (result.getStatus() == ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case Passed i.s : " + result.getName());
		}
	}

	@AfterSuite
	public void endReport() {
//In after suite stopping the object of ExtentReports and ExtentTest
		extentReport.endTest(extentTest);
		extentReport.flush();
		driver.quit();
	}

}

//<------------------------------Reference : https://journeyofquality.wordpress.com/2018/09/20/extent-reports-in-selenium-testng />