package com.safaricom.sfc;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class sfcHomepage {

	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void sfcHomePageSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_title"))).isDisplayed();
		MobileElement sfchomepageTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title")); // Title of
																											// page
		String sfchomepage = sfchomepageTitle.getText();
		String str1 = "MENU123";
		Assert.assertEquals(sfchomepage, str1);
		System.out.println("Landed in SFC Home Page");
		ScreenshotSuccess.screenShotOnCapture(driver);
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		MobileElement banner = driver.findElement(By.id("com.selfcare.safaricom:id/ll_banner_image"));
		if (banner.isDisplayed())
		{
			System.out.println("Banner displayed");
		ScreenshotSuccess.screenShotOnCapture(driver);
		}
		else
		{
			System.out.println("Banner not displayed");
		ScreenshotSuccess.screenShotOnCapture(driver); // call screenShotOnError function
		}

	}

}
