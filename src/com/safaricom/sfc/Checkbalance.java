package com.safaricom.sfc;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class Checkbalance {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();

	@Test
	public void CheckbalanceSuccess() throws InterruptedException {

		// <------------------------------------Checkbalance-------------------------------------------------->
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Check Balance']")))
				.click();

		MobileElement CheckbalanceTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		
		String Checkbalance = CheckbalanceTitle.getText();
		String Str1 = "CHECK BALANCE";
		// Compare expected title with actual title
		Assert.assertEquals(Checkbalance, Str1);
			
		System.out.println("CHECK BALANCE Page In");
		// <-------------------------------------Redeem Bonga
		// Points------------------------------------------->
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_redeem_bonga")))
				.click(); // Redeem Bonga Click
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_redeem_option_field")))
				.click(); // Redeem Option Click

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Minutes']")))
				.click(); // Select redeem option

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_select_amount_field")))
				.click(); // Select Amount click

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='4 min for 50 pts']"))).click(); // Select
																													// Amount

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_redeem"))).click(); // button
																													// click

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click(); // confirmation
																												// ok
																												// click
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click(); // success
																												// ok
																												// click
	//	wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

		
		//<-------------------------------------------------------Buy Data------------------------------------------------------->
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_buy_data"))).click(); //Click Buy Data
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/indicator"))).click(); //
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_activate"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/indicator"))).click(); //
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_activate"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
	}

	

	
	
}
