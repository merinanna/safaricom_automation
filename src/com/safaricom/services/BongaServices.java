package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BongaServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void BongaServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bonga Services']")))
		.click(); //Click Bonga Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "BONGA SERVICES";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in Services Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		//<---------------------Redeem------------------------------------>
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Redeem']")))
		.click(); //Click Redeem
		
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_redeem_option_field")))
		.click(); // Redeem Option Click

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Minutes']")))
		.click(); // Select redeem option

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_select_amount_field")))
		.click(); // Select Amount click

wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.TextView[@text='4 min for 50 pts']"))).click(); // Select
																											// Amount

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_redeem"))).click(); // button
																											// click

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click(); // confirmation
																										// ok
																										// click
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click(); // success
																										// ok
																										// click
//	wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();




//<-----------------------------Transfer Bonga Points---------------------------------------------------->

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_bonga_transfer")))
.click(); // Click Transfer
MobileElement TransferMobileNum = driver.findElement(By.id("com.selfcare.safaricom:id/et_bonga_mobile_num"));
TransferMobileNum.sendKeys("790771777"); //Enter Mobile Number Field
MobileElement NoOfPoints = driver.findElement(By.id("com.selfcare.safaricom:id/et_num_of_points"));
NoOfPoints.sendKeys("100"); //Enter number of points field
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_points_transfer")))
.click(); // Click Transfer button
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok")))
.click(); // Click Ok button
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_pin")))
.sendKeys("1234");//Enter Service Pin

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_ok")))
.click(); // Click Ok button
wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok")))
.click(); // Click Ok button




//<------------------------View Redemption History--------------------------------------------->

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_redemption_history")))
.click(); // Click View Redemption History

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/text_date1")))
.click(); // Click StartDate

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.view.View[@text='3']")))
.click(); //Click 3

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.Button[@text='OK']")))
.click(); //Click Ok

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/text_date2")))
.click(); // Click End Date

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.view.View[@text='3']")))
.click(); //Click 3

wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.Button[@text='OK']")))
.click(); //Click Ok

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_requestforbill")))
.click(); //Click Ok

wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();




//<------------------------Accumulation History-------------------------------------------------->

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_accumulation_history")))
.click(); // Click View Accumulation History

wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();


	}

}
