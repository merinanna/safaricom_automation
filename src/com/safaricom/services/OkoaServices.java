package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OkoaServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void SkizaServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='OKOA SERVICES']")))
		.click(); //Click Sambaza Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "OKOA SERVICES";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in OKOA SERVICES Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		


	}

}
