package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class RoamingServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void SkizaServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Skiza Services']")))
		.click(); //Click Sambaza Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "ROAMING SERVICES";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in ROAMING SERVICES Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		
		//<----------------------------Purchase------------------------------------------------->
		
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_roaming_purchase"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_roaming_purchase"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
		//<----------------------------Roaming Tariff------------------------------------------------->
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_roaming_traiff"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tune_search_btn"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/roaming_traiff_continent_spinner"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Asia']")))
		.click(); //Click Sambaza Services
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/roaming_traiff_country_spinner"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bahrain']")))
		.click(); //Click Sambaza Services
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_ok"))).click();
	
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
	}

}
