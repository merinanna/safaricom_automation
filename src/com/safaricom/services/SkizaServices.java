package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SkizaServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void SkizaServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Skiza Services']")))
		.click(); //Click Sambaza Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "Skiza Services";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in Skiza Services Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		
		//<---------------------------Skiza-------------------------------------------------->
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/rb_artist"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/entr_artist_song"))).sendKeys("A R Rahman"); 
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tune_search_btn"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/subscribe_hottest_tune"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

	}

}
