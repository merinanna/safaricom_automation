package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FlexServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void FlexServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='FLEX']")))
		.click(); //Click Flex Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "FLEX";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in FLEX Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		//<---------------------Monthly Flex----------------------------------->
		
//wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/iv_expand")))
//		.click(); // Click Monthly Flex


wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_buy_bundle")))
		.click(); // Click Buy Bundle

//<---------------------------------Buy Once------------------------------------------------->
wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='Buy Once']"))).click(); // Click Buy Once

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Continue click


wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='Airtime']"))).click(); // Click Pay with Airtime

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Continue click

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Send Click in Confirmation

wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.TextView[@text='OK']"))).click(); // Click Pay with Airtime


//wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

 //<-----------------------------------Auto Renew--------------------------------------->

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_buy_bundle")))
.click(); // Click Buy Bundle
wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='Auto Renew']"))).click(); // Click Buy Once

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Continue click


wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='Airtime']"))).click(); // Click Pay with Airtime

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Continue click

wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Send Click in Confirmation

wait.until(ExpectedConditions
		.elementToBeClickable(By.xpath("//android.widget.TextView[@text='OK']"))).click(); // Click Pay with Airtime


wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

	}

}
