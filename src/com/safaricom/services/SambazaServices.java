package com.safaricom.services;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SambazaServices {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	
	@Test
	public void MySMSServicesSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Sambaza Services']")))
		.click(); //Click Sambaza Services
																													// MPESA
		MobileElement serviceshomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "SAMBAZA SERVICES";
		String serviceshomepage = serviceshomepagetitle.getText();
		Assert.assertEquals(serviceshomepage, str1);
		System.out.println("Landed in SAMBAZA SERVICES Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		
		
		//<----------------------------Airtime Sambaza-------------------------------------------------->
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_airtime_sambaza"))).click(); //Click Airtime Sambaza
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_mobilenumber"))).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_pin"))).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_ok"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
		
		//<-----------------------------Data Sambaza----------------------------------------------------->
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_data_sambaza"))).click(); //Click Data Sambaza
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_mobilenumber"))).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_pin"))).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_ok"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
		
				
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

	}

}
