package com.safaricom.mpesa;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class WithdrawCash {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void WithdrawCashSuccess() throws InterruptedException, IOException {

		Thread.sleep(10000);

		MobileElement landPageTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String landPage = landPageTitle.getText();
		System.out.println(landPage);
			System.out.println("In Withdraw Page");
		// <------------------------------------WithdrawFromAgent-------------------------------------------------->
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/cardViewMPEsa"))).click();
//		MobileElement withdrawCash = driver.findElement(By.xpath("//android.widget.TextView[@text='Withdraw Cash']"));
//		withdrawCash.click();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Withdraw Cash']")))
				.click();
		ScreenshotSuccess.screenShotOnCapture(driver);
		MobileElement WithdrawCashTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String withdrawcash = WithdrawCashTitle.getText();
		String Str1 = "WITHDRAW CASH";
		// Compare expected title with actual title

		Assert.assertEquals(withdrawcash, Str1);

		System.out.println("WithdrawCash Page In");
		ScreenshotSuccess.screenShotOnCapture(driver);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_mobilenumber")))
				.click();
		MobileElement edt_mobilenumber_WFAgent = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_mobilenumber");
		edt_mobilenumber_WFAgent.sendKeys("790771777"); // Enter mobile number

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click();
		MobileElement et_amount_WFAgent = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_amount");
		et_amount_WFAgent.sendKeys("200"); // Enter amount

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/continue_sf"))).click(); // Click
																														// Continue

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // clisk send
		Thread.sleep(5000);

		MobileElement labelMessageID1 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		ScreenshotSuccess.screenShotOnCapture(driver);
		String labelMessage1 = labelMessageID1.getText();
		String Str11 = "Please wait to enter M-PESA PIN";
		// Compare final Confirmaton message

		Assert.assertEquals(labelMessage1, Str11);

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// close
																												// in
																												// Enter
																												// M-Pin
																												// Popup

		// <---------------------------------------
		// WithdrawFromATM------------------------------------------------>

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Withdraw Cash']")))
				.click(); // Click Withdraw Cash
		java.util.List<MobileElement> L1 = driver.findElements(By.id("com.selfcare.safaricom:id/iv_expand_ico")); // Assinging
																													// id
																													// as
																													// list
																													// to
																													// select
																													// ATM

		L1.get(1).click();
		ScreenshotSuccess.screenShotOnCapture(driver);

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_mobilenumber")))
				.click();
		MobileElement edt_mobilenumber_WFAtm = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_mobilenumber");
		edt_mobilenumber_WFAtm.sendKeys("1234");// Enter ATMPIN

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/continue_sf"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click();
		ScreenshotSuccess.screenShotOnCapture(driver);
		Thread.sleep(5000);
		MobileElement labelMessageID2 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
		ScreenshotSuccess.screenShotOnCapture(driver); // message
		String labelMessage2 = labelMessageID2.getText();
		String Str12 = "Please wait to enter M-PESA PIN";
		// Compare final Confirmaton message
		Assert.assertEquals(labelMessage2, Str12);

		
	}

}
