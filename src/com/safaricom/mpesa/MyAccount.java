package com.safaricom.mpesa;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MyAccount {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();

	@Test

	public void MyAccountSuccess() throws InterruptedException {
		// <------------------------------------------My
		// Account--------------------------------------------------------->
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='My Account']")))
				.click();
		Thread.sleep(5000);
		MobileElement MyAccountTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String MyAccount = MyAccountTitle.getText();
		String Str1 = "MY ACCOUNT";
		// Compare expected title with actual title for My Account
		Assert.assertEquals(MyAccount, Str1);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Cost Calculator']")))
				.click();
		Thread.sleep(5000);
		MobileElement CostCalculatorTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String CostCalculator = CostCalculatorTitle.getText();
		String Str11 = "COST CALCULATOR";
		
		// Compare expected title with actual title for Cost Calculator
		Assert.assertEquals(CostCalculator, Str11);
		
		// Send or Withdraw
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvsendWithdrawLabel")))
				.click(); // Click Send/Withdraw
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_send_withdraw_amount")))
				.click(); // Click on Enter amount Field
		MobileElement edt_send_withdraw_amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_send_withdraw_amount");
		edt_send_withdraw_amount.sendKeys("200"); // Enter amount

		// <-------------------------------------- Lipa na
		// M-PESA----------------------------------------------------->

		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvMpesaLabel"))).click(); // Click
																														// Lipa
																														// na
																														// M-Pesa
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_bill_num_or_till_num")))
				.click(); // Click Enter Paybill number or Merchant Till number field
		MobileElement edt_bill_num_or_till_num = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_bill_num_or_till_num");
		edt_bill_num_or_till_num.sendKeys("1234"); // Enter Till number

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_bill_or_till_amount")))
				.click();// CLick Enter Amount filed
		MobileElement edt_bill_or_till_amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_bill_or_till_amount");
		edt_bill_or_till_amount.sendKeys("200"); // Enter Amount field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_check_trans_cost")))
				.click(); // Click Check Transaction Cost

		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click(); // Click
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();																											// back
	}
}
