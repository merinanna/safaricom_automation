package com.safaricom.mpesa;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LipanaMpesa {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();

	@Test

	public void LipanaMPesaSuccess() throws InterruptedException {

		// <---------------------------------------Lipa na
		// M-PESA------------------------------------------------------>

		// <------------------------------------------Pay
		// Bill--------------------------------------------------------->

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Lipa na M-PESA']")))
				.click();// Click Lipa na MPESA
		MobileElement LipanaMpesaTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String LipanaMpesa = LipanaMpesaTitle.getText();
		String Str1 = "LIPA NA M-PESA";
		// Compare expected title with actual title
		Assert.assertEquals(LipanaMpesa, Str1);
		System.out.println("Lipa na MPESA Page In");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvPaybill"))).click(); // CLick
																													// Pay
																													// bill

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_buss_no"))).click(); // Click
																														// Enter
																														// Business
																														// No
																														// field
		MobileElement edt_buss_no_PayBill = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_buss_no");
		edt_buss_no_PayBill.sendKeys("123"); // Send value in Enter Business No Field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_lipa_account_number")))
				.click(); // Click Enter Account Number Field

		MobileElement edt_lipa_account_number_PB = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_lipa_account_number");
		edt_lipa_account_number_PB.sendKeys("790771777"); // Send value in account number field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_bill_amount"))).click(); // Click
																															// amount
																															// field

		MobileElement edt_bill_amount_PB = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_bill_amount");
		edt_bill_amount_PB.sendKeys("200"); // Send value in Amount field
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_bill_continue")))
				.click(); // click Continue button

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // Click Send button
		
		Thread.sleep(10000);
		MobileElement labelMessageID = driver.findElement(By.id("com.selfcare.safaricom:id/succss_message")); //Get Final Confirmation message
		String labelMessage = labelMessageID.getText();
		String Str11 = "Please wait to enter M-PESA PIN";
		//Compare final Confirmaton message
		Assert.assertEquals(labelMessage, Str11);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_cancel"))).click(); // Click
																													// close
																													// in
																													// Enter
																													// M-Pin
																													// Popup

		// <-----------------------------------Buy Goods &
		// Services------------------------------------------------>

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Lipa na M-PESA']"))) // Click
																														// Lipa
																														// na
																														// M-Pesa
				.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvBuygoods"))).click(); // Click
																													// Buy
																													// Goods
																													// &
																													// Services
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_till_no"))).click(); // click
																														// Till
																														// no
																														// field

		MobileElement edt_till_no = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/edt_till_no");
		edt_till_no.sendKeys("790771777"); // Send value in Till No Field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_till_amount"))).click(); // Click
																															// Amount
																															// Field

		MobileElement edt_till_amount_BGS = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_till_amount");
		edt_till_amount_BGS.sendKeys("200"); // Send value in Amount field
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_buygoods_continue")))
				.click(); // Click Continue button

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // Click Send button

		Thread.sleep(5000);
		MobileElement labelMessageID2 = driver.findElement(By.id("com.selfcare.safaricom:id/succss_message")); //Get Final Confirmation message
		String labelMessage2 = labelMessageID2.getText();
		String Str12 = "Please wait to enter M-PESA PIN";
		//Compare final Confirmaton message
		Assert.assertEquals(labelMessage2, Str12);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_cancel"))).click(); // Click
																													// close
																													// in
																													// Enter
																													// M-Pin
																													// Popup

	}
}
