package com.safaricom.mpesa;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FulizaMPESA {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void FulizaMPESASucces() throws InterruptedException, IOException {
		// <---------------------------------------------------Fuliza
		// M-PESA-------------------------------------------->

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Fuliza M-PESA']")))
				.click();
		Thread.sleep(10000);
		MobileElement FulizaMPESATitle = driver.findElement(By.id("com.selfcare.safaricom:id/textTitle"));
		String FulizaMPESA = FulizaMPESATitle.getText();
		String Str1 = "Welcome to Fuliza M-PESA";
		// Compare expected title with actual title for My Account
		Assert.assertEquals(FulizaMPESA, Str1);

		// <---------------------------Fuliza MPESA
		// Optin-------------------------------------------------------------->
		// wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/checkBoxTerms"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.CheckBox"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

	}
}
