package com.safaricom.mpesa;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SendMoney {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void SendMoneySucces() throws InterruptedException, IOException {

		// SoftAssert sa = new SoftAssert();
		MobileElement landPageTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String landPage = landPageTitle.getText();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send Money']")))
				.click(); // Click Send Money
		System.out.println("In Send Money Page");
		// <---------------------------------MPESA
		// -SendMoney_SendToOne------------------------------------------->
//			wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/cardViewMPEsa"))).click();
		// wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/mpesa_cv"))).click();
		ScreenshotSuccess.screenShotOnCapture(driver);
		MobileElement SendMoneyTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String sendmoney = SendMoneyTitle.getText();
		String Str1 = "SEND MONEY";
		Assert.assertEquals(sendmoney, Str1);
		System.out.println("Send Money Page In");
		ScreenshotSuccess.screenShotOnCapture(driver);
		// wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSendToOneToggle"))).click();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSendToOnePhoneNumber")))
				.click();
		ScreenshotSuccess.screenShotOnCapture(driver);
		MobileElement SendToOnePhoneNumber = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/inputSendToOnePhoneNumber");
		SendToOnePhoneNumber.sendKeys("790771777"); // Enter phone number
		wait.until(
				ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSendToOnePhoneAmount")))
				.click();
		ScreenshotSuccess.screenShotOnCapture(driver);
		MobileElement SendToOnePhoneAmount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/inputSendToOnePhoneAmount");
		SendToOnePhoneAmount.sendKeys("200"); // Enter Amount
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSendToOneContinue")))
				.click(); // Click continue
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // Click Send
		Thread.sleep(5000);
		MobileElement labelMessageID1 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		ScreenshotSuccess.screenShotOnCapture(driver);
		String labelMessage1 = labelMessageID1.getText();
		String Str11 = "Please wait to enter M-PESA PIN";
		Assert.assertEquals(labelMessage1, Str11);
		// Compare final Confirmaton message
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
		// close
		// in
		// Enter
		// M-Pin
		// Popup

		// <-------------------------- MPESA -
		// SendMoney_SendToOther--------------------------------------------------->
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/mpesa_cv"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSendToOtherToggle")))
				.click(); // Click Send to Other
		wait.until(
				ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSendToOtherPhoneNumber")))
				.click();
		MobileElement SendToOtherPhoneNumber = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/inputSendToOtherPhoneNumber");
		SendToOtherPhoneNumber.sendKeys("790771777"); // enter phone number
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSendToOtherAmount")))
				.click();
		MobileElement SendToOtherAmount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/inputSendToOtherAmount");
		SendToOtherAmount.sendKeys("200"); // Enter amount

		wait.until(
				ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSendToOtherContinue")))
				.click(); // Click Send to Other Continue

		MobileElement labelMessageID2 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		ScreenshotSuccess.screenShotOnCapture(driver);
		String labelMessage2 = labelMessageID2.getText();
		String Str12 = "Please wait to enter M-PESA PIN";
		// Compare final Confirmaton message

		Assert.assertEquals(labelMessage2, Str12);

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click(); // Click
																													// back

	}

}
