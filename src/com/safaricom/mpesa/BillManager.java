package com.safaricom.mpesa;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BillManager {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void BillManagerSuccess() throws InterruptedException, IOException {

		// <--------------------------------------Bill
		// Manager---------------------------------------------------->
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bill Manager']")))
				.click(); // Click BillManager
		Thread.sleep(5000);
		MobileElement BillManagerTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title")); //Get bill manager title
		String BillManager = BillManagerTitle.getText();
		String Str1 = "MY BILLS";
		//Compare actual title of BillManager with expected title.
		Assert.assertEquals(BillManager, Str1);
		System.out.println("Bill Manager Page In");
		// wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSendToOneToggle"))).click();

		// <--------------------------------------- Manage
		// Bill------------------------------------------------------>
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Manage Bills']"))) 
				.click(); //Click Manage bills

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/imgExpand"))).click(); 
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/etAmount"))).click(); //Click Amount Field

		MobileElement etAmount_MB = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/etAmount");
		etAmount_MB.sendKeys("200"); //Send value in amount field
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvPay"))).click(); // Click Pay button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click(); //Click Continue button in Confirmation
		Thread.sleep(5000);
		
		MobileElement labelMessageID = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); //Get Final Confirmation message
		String labelMessage = labelMessageID.getText();
		String Str11 = "Please wait to enter M-PESA PIN.";
		//Compare final Confirmaton message
		Assert.assertEquals(labelMessage, Str11);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); //Click close in Enter M-Pin Popup
		
		Thread.sleep(5000);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		//com.selfcare.safaricom:id/labelMessage
		
		//Please wait to enter M-PESA PIN
		
				
		
		// <--------------------------------------Frequently
		// Paid--------------------------------------------------->

//		wait.until(
//				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Frequently Paid']")))
//				.click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/imgExpand"))).click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/etBillAmount"))).click();
//
//		MobileElement etBillAmount_FB = (MobileElement) driver
//				.findElementById("com.selfcare.safaricom:id/etBillAmount");
//		etBillAmount_FB.sendKeys("200");
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tvPay"))).click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//		// <-----------------------------------------Save
//		// Bill------------------------------------------------------->
//		// com.selfcare.safaricom:id/buttonPositive
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/editAccountName"))).click();
//
//		MobileElement editAccountName_SB = (MobileElement) driver
//				.findElementById("com.selfcare.safaricom:id/editAccountName");
//		editAccountName_SB.sendKeys("Merin");// Account Name
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/editNickName"))).click();
//		MobileElement editNickName_SB = (MobileElement) driver
//				.findElementById("com.selfcare.safaricom:id/editNickName");
//		editNickName_SB.sendKeys("agf");// Nick Name
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.Button[@text='Save']"))).click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='OK']"))).click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSave"))).click(); // Save
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonCancel"))).click();// Cancel
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/toggleAddReminder")))
//				.click();// Add Reminder
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/radioOnce"))).click();// Remind
//																													// Once
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/radioRepeat"))).click();// Repeat

		// Interval
		// com.selfcare.safaricom:id/spinnerRepeatIntervals

		// com.selfcare.safaricom:id/editReminderMessage //

		// <--------------------------------------------Popular
		// Bill-------------------------------------------------->
//		wait.until(
//				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Popular Bills']")))
//				.click();
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/ivBill"))).click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/etAccountNo"))).click();
//
//		MobileElement etAccountNo_PB = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/etAccountNo");
//		etAccountNo_PB.sendKeys("1234");
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Continue']")))
//				.click();
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/editTextBill"))).click();
//		MobileElement elBillManager41 = (MobileElement) driver
//				.findElementById("com.selfcare.safaricom:id/editTextBill");
//		elBillManager41.sendKeys("200");
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();// Save
//																														// Bill
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/editAccountName"))).click();
//
//		MobileElement accountName_SB = (MobileElement) driver
//				.findElementById("com.selfcare.safaricom:id/editAccountName");
//		accountName_SB.sendKeys("Merin");// Account Name
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/editNickName"))).click();
//
//		MobileElement nickName_SB = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/editNickName");
//		nickName_SB.sendKeys("Anna"); // Nick name
//
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonSave"))).click();// Save

	
	}

}
