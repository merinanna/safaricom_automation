package com.safaricom.mpesa;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoansandSavings {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	@Test
	public void LoansandSavingsSuccess() throws InterruptedException, IOException {

		// <--------------------------------------------LOANS AND
		// SAVINGS------------------------------------------------>
		Thread.sleep(5000);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Loans & Savings']")))
				.click(); // LOANS&SAVINGS

		Thread.sleep(5000);
		MobileElement LoansandSavingsTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String LoansandSavings = LoansandSavingsTitle.getText();
		String Str1 = "LOANS & SAVINGS";
		// Compare expected title with actual title
		Assert.assertEquals(LoansandSavings, Str1);

		// <-----------------------------------------------M-SHWARI----------------------------------------------------->

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/card_mshwari"))).click();
		Thread.sleep(5000);
		// <----------------------------------------Send to M-Shwari
		// Amount--------------------------------------------->

		// <---------------------------------Activate
		// M-Shwari------------------------------------------------------------->

//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click();
//		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

//		Thread.sleep(5000);
		MobileElement MShwariTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String MShwari = MShwariTitle.getText();
		String MStr = "M-SHWARI";
		// Compare expected title with actual title Assert.assertEquals(MShwari, MStr);

		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send to M-Shwari']"))).click(); // Send
																													// //
																													// to
																													// //
																													// M-Shwari
																													// Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_mshwari_amount")))
				.click(); // Click Enter Amount fielld

		MobileElement SendMS_Amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/et_mshwari_amount");
		SendMS_Amount.sendKeys("1000");// Send value to M-Shwari Amount
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click();// Click
																														// //
																														// Continue
																														// //
																														// button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // Click Sendbutton
		Thread.sleep(5000);
		MobileElement labelMessageID1 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage1 = labelMessageID1.getText();
		String Str11 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage1, Str11);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// //
																												// close
																												// // in
																												// //Enter
																												// //
																												// M-Pin
																												// //
																												// Popup
		// <----------------------------------------Withdraw from //
		// MShwari------------------------------------------>
		 Thread.sleep(5000);
		
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Withdraw from\n" + "M-Shwari']")))
				.click();// Withdraw from MShwari
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_mshwari_amount")))
				.click(); // Click Enter Amount Field

		MobileElement WithdrawMS_Amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/et_mshwari_amount");
		WithdrawMS_Amount.sendKeys("1000");// Withdraw value from M-Shwari Amount

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // click
																														// //
																														// continue
																														// //
																														// button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog"))) // Click
																													// //
																													// Send
																													// //
																													// button
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID2 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage2 = labelMessageID2.getText();
		String Str12 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage2, Str12);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// //
																												// close
																												// // in
																												// //
																												// Enter
																												// //
																												// M-Pin
																												// //
																												// Popup

		// <---------------------------------------Lock Savings //
		// Account-------------------------------------------------->
		 Thread.sleep(5000);
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Lock Savings Account']"))).click();
		// Lock // Savings // Account

		// <----------------------------------------Open Lock //
		// Account----------------------------------------------------->
		 Thread.sleep(5000);
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Open Lock Account']"))).click();// Open
																													// //
																													// Lock
																													// //
																													// Account
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/spin_acc_from"))).click(); // Click
																														// //
																														// Select
																														// //
																														// Open
																														// //
																														// Account
																														// //
																														// From

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='M-Shwari']")))
				.click();// //Select M-Shwari

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_target_amount")))
				.click(); // Click Target amount field
		MobileElement target_Amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/et_target_amount");
		target_Amount.sendKeys("1000"); // Send Value in Target Amount field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_period"))).click(); // Click
																													// //
																													// Period
																													// //
																													// field
		MobileElement et_period = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_period");
		et_period.sendKeys("10"); // Send period of time(in months)

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click(); // Click
																													// //
																													// Amount
																													// //
																													// Filed
		MobileElement et_amount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_amount");
		et_amount.sendKeys("100"); // Send value in Amount Field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click(); // Click
																														// //
																														// Continue
																														// //button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click(); // Click Continue button
		Thread.sleep(5000);
		MobileElement labelMessageID3 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage3 = labelMessageID3.getText();
		String Str13 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage3, Str13);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// //
																												// close
																												// // in
																												// //
																												// Enter
																												// //
																												// M-Pin
																												// //
																												// Popup

		// <---------------------------------------------
		// Save--------------------------------------------------------->
		 Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Save']")))
				.click();// Save
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/spin_acc_from"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='M-Shwari']")))
				.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click();
		MobileElement save_amount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_amount");
		save_amount.sendKeys("100");// Save from M-Shwari Amount
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID4 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage4 = labelMessageID4.getText();
		String Str14 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage4, Str14);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

		// <--------------------------------------------
		// Withdraw---------------------------------------------------->
		 Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Withdraw']")))
				.click();// Withdraw
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click();
		MobileElement withdraw_amount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_amount");
		withdraw_amount.sendKeys("100");// Withdraw from M-Shwari Amount
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID5 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage5 = labelMessageID5.getText();
		String Str15 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage5, Str15);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

		// <-------------------------------------------Mini //
		// Statement----------------------------------------------->
		 Thread.sleep(5000);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mini Statement']")))
				.click();// Mini Statement

		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		

		// <-------------------------------------------Loan-------------------------------------------------------->
		
		 Thread.sleep(5000);wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Loan']")))
				.click();
		// Loan // <---------------------------------------Request //
		// Loan----------------------------------------------------->
		
		 Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Request Loan']")))
				.click();// Request Loan
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click();

		MobileElement RequestLoan_Amount = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/et_amount");
		RequestLoan_Amount.sendKeys("1000");// Request Loan Amount

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/loan_btn_text"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID6 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage6 = labelMessageID6.getText();
		String Str16 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage6, Str16);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();

		// <-----------------------------------------------Pay
		// Loan----------------------------------------------------->
		
		 Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Pay Loan']")))
				.click();// Pay Loan

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/spinnerLoanFrom"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='M-Shwari']")))
				.click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_amount"))).click();

		MobileElement PayLoan_Amount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_amount");
		PayLoan_Amount.sendKeys("1000");// Pay Loan Amount
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/loan_btn_text"))).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog")))
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID7 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); // Get
																												// Final
																												// Confirmation
																												// message
		String labelMessage7 = labelMessageID7.getText();
		String Str17 = "Please wait to enter M-PESA PIN."; // Compare final Confirmaton message
		Assert.assertEquals(labelMessage7, Str17);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

		// <----------------------------------------- Mini
		// Statement--------------------------------------------------->

		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mini Statement']")))
				.click(); // Mini Statement
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Deposit Mini Statement']"))).click();
		// Deposit // Mini // Statement
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Loan Mini Statement']"))).click(); // Loan
																													// //
																													// Mini
																													// //
																													// Statement
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//android.widget.TextView[@text='Lock Savings Account Mini Statement']"))).click(); // Lock //
																												// Savings
																												// //
																												// Account
																												// //
																												// Mini
																												// //
																												// Statement
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
		// KCB MPESA

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/card_kcb_mpesa"))).click();
		Thread.sleep(5000);
		// <---------------------------------Activate
		// M-Shwari------------------------------------------------------------->

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/btn_continue"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
	}

}
