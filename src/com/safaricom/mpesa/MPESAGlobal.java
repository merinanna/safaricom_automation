package com.safaricom.mpesa;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.screenshot.ScreenshotSuccess;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;

@Test
public class MPESAGlobal {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	public static ScreenshotSuccess screenShotObject = new ScreenshotSuccess();

	public void MPESAGlobalSucces() throws InterruptedException, IOException {

		// <------------------------------------------------M-PESA
				// Global----------------------------------------------->

				wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='M-PESA Global']")))
						.click();
				Thread.sleep(5000);
			MobileElement MPESAGLobalTitle=driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
			String MPESAGLobal = MPESAGLobalTitle.getText();
			String Str1 = "M-PESA GLOBAL";
			// Compare expected title with actual title for My Account
			Assert.assertEquals(MPESAGLobal, Str1);
			
			//<------------------------Scoll---------------------------------------------------------->
			Thread.sleep(5000);
			MobileElement location=driver.findElement(By.id("com.selfcare.safaricom:id/location"));
			location.sendKeys("IND");
			wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/checkBox_terms_cond"))).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_ok"))).click();
			
			wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
			
	//<---------------------------------------------------------------------------------------------------------------------------------------------->
//				// <------------------------Send-MobileNumber-Rwanda-Source of fund :
//				// Salary-------------------------------->
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Rwanda']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_RwandaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_RwandaSMSal.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_RwandaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_RwandaSMSal.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Salary']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Rwanda-Source of fund :
//				// Business-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Rwanda']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_RwandaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_RwandaSMB.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_RwandaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_RwandaSMB.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Business']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Rwanda-Source of fund :
//				// Other-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Rwanda']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_RwandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_RwandaSMO.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_RwandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_RwandaSMO.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputRemarks"))).click();
//
//				MobileElement inputRemarks_RwandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputRemarks");
//				inputRemarks_RwandaSMO.sendKeys("FD");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// com.selfcare.safaricom:id/button //MPESA PIN
//
//				// <------------------------Send-MobileNumber-Tanzania-Source of fund :
//				// Salary-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Tanzania']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_TanzaniaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_TanzaniaSMSal.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_TanzaniaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_TanzaniaSMSal.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Salary']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Tanzania-Source of fund :
//				// Business-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Tanzania']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_TanzaniaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_TanzaniaSMB.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_TanzaniaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_TanzaniaSMB.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Business']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Tanzania-Source of fund :
//				// Other-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Tanzania']")))
//						.click();
//				;
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_TanzaniaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_TanzaniaSMO.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_TanzaniaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_TanzaniaSMO.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputRemarks"))).click();
//
//				MobileElement inputRemarks_TanzaniaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputRemarks");
//				inputRemarks_TanzaniaSMO.sendKeys("FD");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Uganda-Source of fund :
//				// Salary-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Uganda']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_UgandaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_UgandaSMSal.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_UgandaSMSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_UgandaSMSal.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Salary']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Uganda-Source of fund :
//				// Business-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Uganda']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_UgandaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_UgandaSMB.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_UgandaSMB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_UgandaSMB.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Business']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-MobileNumber-Uganda-Source of fund :
//				// Other-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Mobile Number']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Uganda']")))
//						.click();
//				;
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputNumber"))).click();
//				MobileElement inputNumber_UgandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputNumber");
//				inputNumber_UgandaSMO.sendKeys("790771777");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_UgandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_UgandaSMO.sendKeys("200");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputRemarks"))).click();
//
//				MobileElement inputRemarks_UgandaSMO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputRemarks");
//				inputRemarks_UgandaSMO.sendKeys("FD");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-Bank Account-Source of fund :
//				// Salary-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bank Account']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='United Kingdom']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='British Pound']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement inputFirstName = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				inputFirstName.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement inputMiddleName = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				inputMiddleName.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement inputLastName = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputLastName");
//				inputLastName.sendKeys("Mathew");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAddress"))).click();
//				MobileElement inputAddress = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAddress");
//				inputAddress.sendKeys("KalathilParambil House");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputBankName"))).click();
//				MobileElement inputBankName = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputBankName");
//				inputBankName.sendKeys("ICICI");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSwiftCodeIBAN")))
//						.click();
//				MobileElement inputSwiftCodeIBAN = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputSwiftCodeIBAN");
//				inputSwiftCodeIBAN.sendKeys("ICICI0002534");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAccountNumber")))
//						.click();
//				MobileElement inputAccountNumber = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAccountNumber");
//				inputAccountNumber.sendKeys("25340515199");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount.sendKeys("1000");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputReference"))).click();
//				MobileElement ref = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputReference");
//				ref.sendKeys("Reference");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Salary']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement remark_s = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				remark_s.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------Send-Bank Account-Source of fund :
//				// Business-------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bank Account']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='United Kingdom']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='British Pound']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement firstName_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				firstName_b.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement midName_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				midName_b.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement lastName_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputLastName");
//				lastName_b.sendKeys("Mathew");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAddress"))).click();
//				MobileElement address_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAddress");
//				address_b.sendKeys("KalathilParambil House");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputBankName"))).click();
//				MobileElement bankName_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputBankName");
//				bankName_b.sendKeys("ICICI");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSwiftCodeIBAN")))
//						.click();
//				MobileElement swiftCode_b = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputSwiftCodeIBAN");
//				swiftCode_b.sendKeys("ICICI0002534");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAccountNumber")))
//						.click();
//				MobileElement acNumber_b = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAccountNumber");
//				acNumber_b.sendKeys("25340515199");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement amount_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				amount_b.sendKeys("1000");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputReference"))).click();
//				MobileElement ref_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputReference");
//				ref_b.sendKeys("Reference");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Business']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement remark_b = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				remark_b.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// Bank Account //Other
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Bank Account']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='United Kingdom']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='British Pound']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement firstName_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				firstName_o.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement midName_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				midName_o.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement lastName_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputLastName");
//				lastName_o.sendKeys("Mathew");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAddress"))).click();
//				MobileElement address_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAddress");
//				address_o.sendKeys("KalathilParambil House");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputBankName"))).click();
//				MobileElement bankName_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputBankName");
//				bankName_o.sendKeys("ICICI");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSwiftCodeIBAN")))
//						.click();
//				MobileElement swiftCode_o = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputSwiftCodeIBAN");
//				swiftCode_o.sendKeys("ICICI0002534");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAccountNumber")))
//						.click();
//				MobileElement acNumber_o = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAccountNumber");
//				acNumber_o.sendKeys("25340515199");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement amount_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				amount_o.sendKeys("1000");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputReference"))).click();
//				MobileElement ref_o = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputReference");
//				ref_o.sendKeys("Reference");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSource"))).click();
//				MobileElement source_0 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputSource");
//				source_0.sendKeys("Nil");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement purpose_0 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				purpose_0.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// com.selfcare.safaricom:id/button //MPESA PIN
//
//				// Western Union Loaction
//
//				// Western Union Location //Salary
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Western Union Location']"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Afghanistan']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='US Dollar']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement inputFirstName_WSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				inputFirstName_WSal.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement inputMiddleName_WSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				inputMiddleName_WSal.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement inputLastName_WSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputLastName");
//				inputLastName_WSal.sendKeys("Mathew");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_WSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_WSal.sendKeys("1000");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Salary']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement inputPurpose_WSal = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				inputPurpose_WSal.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// Western Union Location //Business
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Western Union Location']"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Afghanistan']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='US Dollar']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement inputFirstName_WB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				inputFirstName_WB.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement inputMiddleName_WB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				inputMiddleName_WB.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement inputLastName_WB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputLastName");
//				inputLastName_WB.sendKeys("Mathew");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_WB = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_WB.sendKeys("1000");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Business']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement inputPurpose_WB = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				inputPurpose_WB.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// Western Union Location //Other
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send']")))
//						.click();
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Western Union Location']"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Afghanistan']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='US Dollar']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputFirstName"))).click();
//				MobileElement inputFirstName_WO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputFirstName");
//				inputFirstName_WO.sendKeys("Merin");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputMiddleName"))).click();
//				MobileElement inputMiddleName_WO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputMiddleName");
//				inputMiddleName_WO.sendKeys("Anna");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputLastName"))).click();
//				MobileElement inputLastName_WO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputLastName");
//				inputLastName_WO.sendKeys("Mathew");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement inputAmount_WO = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				inputAmount_WO.sendKeys("1000");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSource"))).click();
//				MobileElement inputSource_WO = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputSource");
//				inputSource_WO.sendKeys("Nil");//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputPurpose"))).click();
//				MobileElement inputPurpose_WO = (MobileElement) driver
//						.findElementById("com.selfcare.safaricom:id/inputPurpose");
//				inputPurpose_WO.sendKeys("Nil");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <------------------------------------------PAYPAL------------------------------------------------------------>
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Paypal']")))
//						.click();
//				// <---------------------------------- Top Up Paypal
//				// Account----------------------------------------------------->
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Top Up Paypal Account']"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement payPalAmount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				payPalAmount.sendKeys("1000");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();
//
//				// <-----------------------------------Withdraw from PayPal
//				// Account----------------------------------------------->
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Paypal']")))
//						.click();
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Withdraw from Paypal Account']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//
//				// <-----------------------------------------COST
//				// ESTIMATOR-------------------------------------------------------->
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Cost Estimator']")))
//						.click();
//				// <----------------------------------------Send to Mobile
//				// Number-------------------------------------------------->
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send to Mobile Number']"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Rwanda']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement Amount = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				Amount.sendKeys("1000");
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
//
//				// <----------------------------------------- Send to
//				// bank------------------------------------------------------>
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Send to Bank Account']"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='United Kingdom']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='British Pound']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement amount1 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				amount1.sendKeys("100");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
//
//				// <---------------------------------- Select Western Union
//				// Location------------------------------------------->
//
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Western Union Location']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Country']")))
//						.click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputSearchText"))).click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Afghanistan']")))
//						.click();
//
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Select Currency']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='US Dollar']")))
//						.click();
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/inputAmount"))).click();
//				MobileElement amount2 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/inputAmount");
//				amount2.sendKeys("100");
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonContinue"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/buttonPositive"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
//				wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();
//
//				// <---------------------------------------------OPT
//				// OUT------------------------------------------------------------->
//
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Opt Out ']")))
//						.click();
//				wait.until(
//						ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Not interested']")))
//						.click();// Not interested
//				wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Transaction Charges are High']")))
//						.click();// Transaction Charges are High
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Other']")))
//						.click();// Other
//				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='SUBMIT']")))
//						.click();// SUBMIT
//				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click();// MPESA
//																														// PIN

	}

}
