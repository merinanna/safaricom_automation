package com.safaricom.mpesa;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.safaricom.config.TestConfig;
import com.safaricom.mpesa.LipanaMpesa;
import com.safaricom.screenshot.ScreenshotSuccess;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class mpesaHomepage {
	
	public static ScreenshotSuccess screenshotObject = new ScreenshotSuccess();
	

	@Test
	public void mpesaHomePageSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait();
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
		//SoftAssert sa = new SoftAssert();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/cardViewMPEsa"))).click(); // Click
																														// MPESA
		MobileElement mpesahomepagetitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String str1 = "M-PESA12";
		String mpesahomepage = mpesahomepagetitle.getText();
		Assert.assertEquals(mpesahomepage, str1);
		System.out.println("Landed in MPESA Home Page");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		MobileElement scanQR = driver.findElement(By.id("com.selfcare.safaricom:id/buttonScanQR")); // Scan QR button
		scanQR.isDisplayed();
		System.out.println("scanQR button displayed");
		ScreenshotSuccess.screenShotOnCapture(driver);

	//	wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

	}

}
