package com.safaricom.mpesa;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.safaricom.config.TestConfig;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BuyAirtime {

	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();

	@Test
	public void BuyAirtimeSuccess() throws InterruptedException {

		// <---------------------------------------- Buy
		// Airtime--------------------------------------------------->

		// wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/cardViewMPEsa"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Buy Airtime']"))) // Click
																														// BuyAirtime
				.click();
		MobileElement BuyAirtimeTitle = driver.findElement(By.id("com.selfcare.safaricom:id/txt_title"));
		String buyAirtime = BuyAirtimeTitle.getText();
		String Str1 = "BUY AIRTIME";
		// Compare actual title of Buy Airtime with expected title
		Assert.assertEquals(buyAirtime, Str1);
		System.out.println("Buy Airtime Page In");

		// <---------------------------------------Buy Own
		// Number-------------------------------------------------->

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='My Number']")))
				.click(); // Clcik My Number radiobutton
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_pin"))).click();
		MobileElement et_pin = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_pin"); // Clikc
																											// Enter
																											// amount
																											// field
		et_pin.sendKeys("200"); // Send value

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_topup"))).click(); // Click
																													// continue
																													// button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog"))) //// Click
																													//// Send
																													//// in
																													//// confirmation
																													//// pop
																													//// up
				.click();
		Thread.sleep(5000);		
		MobileElement labelMessageID = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); //Get Final Confirmation message
		String labelMessage = labelMessageID.getText();
		String Str11 = "Please wait to enter M-PESA PIN";
		//Compare final Confirmaton message
		Assert.assertEquals(labelMessage, Str11);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// close
																												// in
																												// Enter
																												// M-Pin
																												// Popup

		// <--------------------------------------Buy Other
		// Number------------------------------------------------->
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[@text='Buy Airtime']"))) // Click
																														// Buy
																														// Airtime
				.click();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.RadioButton[@text='Other Number']"))) // Click
																															// Other
																															// number
																															// radio
																															// button
				.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/edt_mobilenumber")))
				.click(); // Click Phone number field

		MobileElement edt_mobilenumber_BuyAirtimeOther = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/edt_mobilenumber");
		edt_mobilenumber_BuyAirtimeOther.sendKeys("790771777"); // Send value Phone number field
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_pin"))).click(); // click
																												// amount
																												// field

		MobileElement et_pin_BuyAirtimeOther = (MobileElement) driver
				.findElementById("com.selfcare.safaricom:id/et_pin");
		et_pin_BuyAirtimeOther.sendKeys("200"); // Send value in Amount field

		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_topup"))).click(); // click
																													// Continue
																													// button
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/txt_continue_dilaog"))) // Click
																													// Send
																													// button
				.click();
		Thread.sleep(5000);
		MobileElement labelMessageID2 = driver.findElement(By.id("com.selfcare.safaricom:id/labelMessage")); //Get Final Confirmation message
		String labelMessage2 = labelMessageID2.getText();
		String Str12 = "Please wait to enter M-PESA PIN";
		//Compare final Confirmaton message
		Assert.assertEquals(labelMessage2, Str12, "Confirmation Message not similar");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/button"))).click(); // Click
																												// close
																												// in
																												// Enter
																												// M-Pin
																												// Popup
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

	}

}
