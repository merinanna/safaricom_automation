package com.safaricom.login;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.safaricom.config.TestConfig;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginTest {

	File csv_path = new File(System.getProperty("user.dir") + "/sfc.csv");// "/home/merin/Merin/Safaricom_Automation/eclipse-workspace/sfc.csv";//
	// public static sfcHomepage sfchomeObject=new sfcHomepage();

	public void LoginSuccess() throws InterruptedException, IOException {

		WebDriverWait wait = TestConfig.getInstance().getWait(); // Initialise wait
		AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver(); // Initialise driver

		System.out.println("In login Succes Page");

		wait.until(ExpectedConditions
				.elementToBeClickable(By.id("com.android.packageinstaller:id/permission_allow_button"))).click();
		wait.until(ExpectedConditions
				.elementToBeClickable(By.id("com.android.packageinstaller:id/permission_allow_button"))).click();
		// Csv Reader

		CSVReader reader = new CSVReader(new FileReader(csv_path));
		String[] csvCell;
		// while loop will be executed till the last line In CSV.
		while ((csvCell = reader.readNext()) != null) {
			String segment = csvCell[0];
			String msisdn = csvCell[1];
			String amount = csvCell[2];
			String id = csvCell[3];
			//System.out.println("msisdn : " + msisdn + "amount : " + amount + "id : " + id);
			if (segment.equals("blaze")) {
				String msidnSegment = csvCell[1];
				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_autofill_phn")))
						.click();
				MobileElement el1 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_autofill_phn"); // Account
				System.out.println(el1);
				el1.sendKeys(msidnSegment); // number
				// el1.sendKeys("90771777");

				MobileElement el2 = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/tv_generate_pin"); // GeneratePin
																															// button
				el2.click();
//					wait.until(ExpectedConditions
//							.elementToBeClickable(By.id("com.android.packageinstaller:id/permission_allow_button"))).click();
				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/et_enter_otp")))
						.click();
				MobileElement otp = (MobileElement) driver.findElementById("com.selfcare.safaricom:id/et_enter_otp"); // Enter
																														// otp
																														// field
				otp.sendKeys("1234");
				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/term_condition")))
						.click(); // Click
									// terms
									// &
									// conditions
				wait.until(ExpectedConditions.elementToBeClickable(By.id("com.selfcare.safaricom:id/tv_otp_login")))
						.click(); // click
									// otp
									// login
									// button
				// wait.until(ExpectedConditions.elementToBeClickable(By.className("android.widget.ImageButton"))).click();

				// sfchomeObject.sfcHomePageSuccess();
			} else
			{
				//System.out.println("Segment not same");
		}

	}
	}}
